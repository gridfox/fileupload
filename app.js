const express = require('express');
const multer = require('multer');
const ejs = require('ejs');
const path = require('path');

//set storage engine
const storage = multer.diskStorage({
    destination: './public/uploads',
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() + 
        path.extname(file.originalname));
    }
});


//sinit upload
const upload = multer({
    storage : storage,
    limits:{fileSize:10000000},
    fileFilter:function(req,file, cb){
        checkFileType(file, cb);
    }
}).single('myImage');

//check file type
function checkFileType(file, cb){
    // allowed extention
    const fileTypes =  /jpeg|jpg|png|gif/;
    //check ext
    const extname = fileTypes.test(path.extname(file.originalname).toLowerCase());
    //check mime
    const mimeType = fileTypes.test(file.mimetype);
    if(mimeType && extname){
        return cb(null, true);
    }else{
        cb('Error: Images only. jpg, png, gif etc');
    }
}
//init app
const app = express();

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname,'public')));

app.get('/',(req,res)=>{
    res.render('index');
});
app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
      if(err){
        res.render('index', {
          msg: err
        });
      } else {
        if(req.file == undefined){
          res.render('index', {
            msg: 'Error: No File Selected!'
          });
        } else {
          res.render('index', {
            msg: 'File Uploaded!',
            file: `uploads/${req.file.filename}`
          });
        }
      }
    });
  });
  
  


const port = 3000;
app.listen(port,()=> console.log(`server started on port ${port}`))